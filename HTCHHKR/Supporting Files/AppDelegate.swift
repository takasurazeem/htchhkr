//
//  AppDelegate.swift
//  HTCHHKR
//
//  Created by Takasur Azeem on 09/10/2019.
//  Copyright © 2019 semangix. All rights reserved.
//

import UIKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    fileprivate var containerVC = ContainerVC()
    
    var menuContainerVC : ContainerVC {
        return containerVC
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        FirebaseApp.configure()
        Database.database().isPersistenceEnabled = true
        
        
        containerVC = ContainerVC()
        window?.rootViewController = containerVC
        window?.makeKeyAndVisible()
        
        return true
    }
    
    
    class func getAppDelegate() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
}

