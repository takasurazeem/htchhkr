//
//  UpdateService.swift
//  HTCHHKR
//
//  Created by Takasur Azeem on 25/04/2020.
//  Copyright © 2020 semangix. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import Firebase

class UpdateService {
    
    private init() { }
    
    static let instance = UpdateService()
    
    func updateUserLocation(withCoordinate coordinate: CLLocationCoordinate2D) {
        DataService.instance.REF_USERS.observeSingleEvent(of: .value) { [weak self] (snapshot) in
            guard self != nil else {return}
            guard let userSnapshot = snapshot.children.allObjects as? [DataSnapshot] else { return }
            for user in userSnapshot {
                if user.key == Auth.auth().currentUser?.uid {
                    DataService.instance.REF_USERS.child(user.key).updateChildValues(["coordinates" : [coordinate.latitude, coordinate.longitude]])
                }
            }
        }
    }
    
    func updateDriverLocation(withCoordinate coordinate: CLLocationCoordinate2D)  {
        DataService.instance.REF_DRIVERS.observeSingleEvent(of: .value) { (snapShot) in
            guard let driverSnapshot = snapShot.children.allObjects as? [DataSnapshot] else { return }
            for driver in driverSnapshot {
                if driver.key == Auth.auth().currentUser?.uid {
                    guard let isPickupModeEnabled = driver.childSnapshot(forPath: "isPickupModeEnabled").value as? Bool else { return  }
                    if isPickupModeEnabled {
                        DataService.instance.REF_DRIVERS.child(driver.key).updateChildValues(["coordinates": [coordinate.latitude, coordinate.longitude]])
                    }
                }
            }
        }
    }
}
