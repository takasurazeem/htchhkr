//
//  GradientView.swift
//  HTCHHKR
//
//  Created by Takasur Azeem on 13/10/2019.
//  Copyright © 2019 semangix. All rights reserved.
//

import UIKit

@IBDesignable
class GradientView: UIView {

    let gradient = CAGradientLayer()
    
    override func awakeFromNib() {
        setupGradientView()
    }
    
    func setupGradientView() {
        gradient.frame = self.bounds
        gradient.colors = [UIColor.white.cgColor, UIColor.init(white: 1.0, alpha: 0.0).cgColor]
        gradient.startPoint = .zero
        gradient.endPoint = CGPoint(x: 0, y: 1)
        gradient.locations = [0.8,  1.0]
        self.layer.addSublayer(gradient)
    }
    
}
