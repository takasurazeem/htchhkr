//
//  RoundedShadowButton.swift
//  HTCHHKR
//
//  Created by Takasur Azeem on 13/10/2019.
//  Copyright © 2019 semangix. All rights reserved.
//

import UIKit
import SSSpinnerButton

class RoundedShadowButton: SSSpinnerButton {
 
//    var originalSize: CGRect?
    
    
    override func awakeFromNib() {
        setupView()
    }

    func setupView() {
//        self.originalSize = self.frame
        self.layer.cornerRadius = 5
        self.layer.shadowRadius = 10
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowOpacity = 0.3
        self.layer.shadowOffset = .zero
    }
    
    /*
    func animateButton(shouldLoad: Bool, withMessage message: String?) {
        // TODO: Fix animation bug
        let spinner = UIActivityIndicatorView(style: .large)
        spinner.color = .darkGray
        spinner.alpha = 0.0
        spinner.hidesWhenStopped = true
        spinner.tag = 21
        
        if shouldLoad {
            self.addSubview(spinner)
            self.setTitle("", for: .normal)
            UIView.animate(withDuration: 0.2, animations: {
                self.frame = CGRect(x: self.frame.midX - (self.frame.height / 2), y: self.frame.origin.y, width: self.frame.height , height: self.frame.height)
                self.layer.cornerRadius = self.frame.height / 2
            }) { (finished) in
                if finished == true {
                    spinner.startAnimating()
                    spinner.center = CGPoint(x: self.frame.width / 2 + 1, y: self.frame.width / 2 + 1)
                    spinner.fadeTo(alphaValue: 1.0, withDuration: 0.2)
                }
            }
            self.isUserInteractionEnabled = false
        } else {
            self.isUserInteractionEnabled = true
            for subview in self.subviews {
                if subview.tag == 21 {
                    subview.removeFromSuperview()
                }
            }
            UIView.animate(withDuration: 0.2) {
                self.layer.cornerRadius =  5.0
                self.frame = self.originalSize!
                self.setTitle(message, for: .normal)
            }
        }
    }
 */
}
