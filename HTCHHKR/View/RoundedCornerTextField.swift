//
//  RoundedCornerTextField.swift
//  HTCHHKR
//
//  Created by Takasur Azeem on 17/12/2019.
//  Copyright © 2019 semangix. All rights reserved.
//

import UIKit

class RoundedCornerTextField: UITextField {

    override func awakeFromNib() {
        setupView()
    }
    
//    var textRectOffset: CGFloat = 10
    
    func setupView() {
        self.layer.cornerRadius = self.frame.height / 2
        self.clipsToBounds = true
    }
    
    let padding = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)

    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

}
