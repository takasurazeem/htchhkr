//
//  CircleView.swift
//  HTCHHKR
//
//  Created by Takasur Azeem on 13/10/2019.
//  Copyright © 2019 semangix. All rights reserved.
//

import UIKit

@IBDesignable
class CircleView: UIView {

    override func awakeFromNib() {
        setupView()
    }
    
    func setupView() {
        self.layer.cornerRadius = self.frame.width / 2
        self.layer.borderColor = #colorLiteral(red: 0.2235294118, green: 0.2235294118, blue: 0.2235294118, alpha: 1)
        self.layer.borderWidth = 1.5
    }

}
