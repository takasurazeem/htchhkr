//
//  RoundedShadowView.swift
//  HTCHHKR
//
//  Created by Takasur Azeem on 13/10/2019.
//  Copyright © 2019 semangix. All rights reserved.
//

import UIKit

@IBDesignable
class RoundedShadowView: UIView {

    override func awakeFromNib() {
        setupView()
    }
    
    func setupView() {
        self.layer.shadowOpacity = 0.3
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowRadius = 5
        self.layer.shadowOffset = CGSize(width: 0, height: 5)
        self.layer.cornerRadius = 5
    }

}
