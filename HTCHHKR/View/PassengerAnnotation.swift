//
//  PassengerAnnotation.swift
//  HTCHHKR
//
//  Created by Takasur Azeem on 27/04/2020.
//  Copyright © 2020 semangix. All rights reserved.
//

import Foundation
import MapKit

class PassengerAnnotation: NSObject, MKAnnotation {
    dynamic var coordinate: CLLocationCoordinate2D
    var key: String
    var title: String?
    
    init(coordinate: CLLocationCoordinate2D, key: String) {
        self.coordinate = coordinate
        self.key = key
        super.init()
    }
}
