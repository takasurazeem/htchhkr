//
//  RoundImageView.swift
//  HTCHHKR
//
//  Created by Takasur Azeem on 13/10/2019.
//  Copyright © 2019 semangix. All rights reserved.
//

import UIKit

@IBDesignable
class RoundImageView: UIImageView {

    override func awakeFromNib() {
        setupView()
    }
    
    func setupView() {
        self.layer.cornerRadius = self.frame.width / 2
        self.clipsToBounds = true
    }

}
