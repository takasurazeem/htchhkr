//
//  ViewController.swift
//  HTCHHKR
//
//  Created by Takasur Azeem on 09/10/2019.
//  Copyright © 2019 semangix. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import RevealingSplashView
import Firebase

class HomeVC: UIViewController {
    
    var delegate: CenterVCDelegate?
    var manager: CLLocationManager?
    var regionRadius: CLLocationDistance = 1000
    
    lazy var uid = Auth.auth().currentUser?.uid
    
    let revealingSplashView = RevealingSplashView(iconImage: UIImage(named: "launchScreenIcon")!, iconInitialSize: CGSize(width: 80, height: 80), backgroundColor: .white)
    
    @IBOutlet weak var actionButton: RoundedShadowButton!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var centerMapButton: UIButton!
    @IBOutlet weak var destinationTextField: UITextField!
    @IBOutlet weak var destinationCircleView: CircleView!
    
    let tableView = UITableView()
    
    var matchingItems: [MKMapItem] = []
    var route: MKRoute!
    
    //    @IBOutlet weak var requestRideButtonLeadingConstraint: NSLayoutConstraint!
    //    @IBOutlet weak var requestRideButtonTrailingConstraint: NSLayoutConstraint!
    
    //MARK:- SplashView
    fileprivate func configureSplashView() {
        self.view.addSubview(revealingSplashView)
        revealingSplashView.animationType = .heartBeat
        revealingSplashView.startAnimation()
        
        revealingSplashView.heartAttack = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureSplashView()
        
        //MARK: Set Delegates
        destinationTextField.delegate = self
        
        //MARK: CoreLocation
        manager = CLLocationManager()
        manager?.delegate = self
        manager?.desiredAccuracy = kCLLocationAccuracyBest
        
        checkLocationAuthStatus()
        
        mapView.delegate = self
        mapView.userLocation.title = nil
        
        centerMapOnUserLocation()
        
        DataService.instance.REF_DRIVERS.observe(.value) { [weak self] (_) in
            guard let self = self else { return }
            self.loadDriversAnnotationFromFB()
        }
    }
    
    func checkLocationAuthStatus() {
        switch CLLocationManager.authorizationStatus() {
        case .authorizedAlways, .authorizedWhenInUse:
            manager?.startUpdatingLocation()
            break
        default:
            manager?.requestAlwaysAuthorization()
        }
    }
    
    func loadDriversAnnotationFromFB() {
        DataService.instance.REF_DRIVERS.observeSingleEvent(of: .value) { [weak self] (snapshot) in
            guard let self = self else { return }
            guard let driverSnapshot = snapshot.children.allObjects as? [DataSnapshot] else { return }
            for driver in driverSnapshot {
                if driver.hasChild("userIsDriver") {
                    if driver.hasChild("coordinates") {
                        guard let isPickupModeEnabled = driver.childSnapshot(forPath: "isPickupModeEnabled").value as? Bool else { return  }
                        if isPickupModeEnabled {
                            guard let driverDict = driver.value as? [String: AnyObject] else { fatalError("Did not convert") }
                            let coordinate = driverDict["coordinates"] as! NSArray
                            let driverCoordinate = CLLocationCoordinate2D(latitude: coordinate[0] as! CLLocationDegrees, longitude: coordinate[1] as! CLLocationDegrees)
                            
                            let annotation = DriverAnnotation(coordinate: driverCoordinate, withKey: driver.key)
                            
                            var isDriverVisible: Bool {
                                return self.mapView.annotations.contains { (annotation) -> Bool in
                                    if let driverAnnotation = annotation as? DriverAnnotation {
                                        if driverAnnotation.key == driver.key {
                                            driverAnnotation.update(annotationPosition: driverAnnotation, withCoordinate: driverCoordinate)
                                            return true
                                        }
                                    }
                                    return false
                                }
                            }
                            
                            if !isDriverVisible {
                                self.mapView.addAnnotation(annotation)
                            }
                        } else {
                            for annotation in self.mapView.annotations {
                                if annotation.isKind(of: DriverAnnotation.self) {
                                    if let annotation = annotation as? DriverAnnotation {
                                        if annotation.key == driver.key {
                                            self.mapView.removeAnnotation(annotation)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func centerMapButtonWasPressed(_ sender: UIButton) {
        centerMapButton.fadeTo(alphaValue: 0.0, withDuration: 0.2)
        centerMapOnUserLocation()
    }
    
    func centerMapOnUserLocation() {
        let coordinateRegion = MKCoordinateRegion.init(center: mapView.userLocation.coordinate, latitudinalMeters: regionRadius * 2.0, longitudinalMeters: regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    @IBAction func actionBtnWasPressed(_ sender: RoundedShadowButton) {
        actionButton.startAnimate(spinnerType: .ballRotateChase, spinnercolor: .gray, complete: nil)
    }
    
    @IBAction func menuButtonWasPressed(_ sender: UIButton) {
        print(#function)
        self.delegate?.toggleLeftPanel()
    }
}

extension HomeVC: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        //        checkLocationAuthStatus()
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            mapView.showsUserLocation = true
            mapView.userTrackingMode = .follow
            break
        default:
            print("")
        }
    }
}

// MARK: MKMapViewDelegate Delegate Extension
extension HomeVC: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        UpdateService.instance.updateUserLocation(withCoordinate: userLocation.coordinate)
        UpdateService.instance.updateDriverLocation(withCoordinate: userLocation.coordinate)
    }
    
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if let annotation = annotation as? DriverAnnotation {
            let identifier = "driver"
            let view: MKAnnotationView
            view = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            view.image = UIImage(named: "driverAnnotation")
            return view
        } else if let annotation = annotation as? PassengerAnnotation {
            let identifier = "passenger"
            let view = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            view.image = #imageLiteral(resourceName: "currentLocationAnnotation")
            view.isDraggable = true
            return view
        } else if let annotation = annotation as? DestinationAnnotation {
            let identifier = "destination"
            let view = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            view.image = #imageLiteral(resourceName: "destinationAnnotation")
            view.isDraggable = true
            return view
        }
        return nil
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let lineRenderer = MKPolylineRenderer(overlay: self.route.polyline)
        lineRenderer.strokeColor = UIColor(displayP3Red: 216/255, green: 71/255, blue: 30/255, alpha: 0.75)
        lineRenderer.lineWidth = 3.0
        lineRenderer.lineCap = .butt
        lineRenderer.lineJoin = .bevel
        return lineRenderer
    }
    
    func mapView(_ mapView: MKMapView, regionWillChangeAnimated animated: Bool) {
        centerMapButton.fadeTo(alphaValue: 1.0, withDuration: 0.2)
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, didChange newState: MKAnnotationView.DragState, fromOldState oldState: MKAnnotationView.DragState) {
        guard let uid = self.uid else { return }
        if view.annotation?.isKind(of: PassengerAnnotation.self) ?? false {
            DataService.instance.REF_USERS.child(uid).updateChildValues(["coordinates" : [view.annotation?.coordinate.latitude, view.annotation?.coordinate.longitude]])
        } else if view.annotation?.isKind(of: DestinationAnnotation.self) ?? false {
            DataService.instance.REF_USERS.child(uid).updateChildValues(["tripCoordinate" : [view.annotation?.coordinate.latitude, view.annotation?.coordinate.longitude]])
        }
    }
    
    func performSearch() {
        matchingItems.removeAll()
        
        let request = MKLocalSearch.Request()
        request.naturalLanguageQuery = destinationTextField.text
        request.region = mapView.region
        
        let search = MKLocalSearch(request: request)
        search.start { [weak self] (response, error) in
            guard let self = self else { return }
            if let error = error {
                print(error.localizedDescription)
            } else if let response = response, response.mapItems.count == 0 {
                print("No results")
            } else {
                guard let mapItems = response?.mapItems else { return }
                for mapItem in mapItems {
                    self.matchingItems.append(mapItem)
                }
                self.tableView.reloadData()
            }
        }
    }
    
    func removeExistingOverlays() {
        mapView.removeOverlays(mapView.overlays)
    }
    
    func searchMapKitForResultsWithPolyLine(for destination: MKMapItem) {
        let request = MKDirections.Request()
        request.source = MKMapItem.forCurrentLocation()
        request.destination = destination
        request.transportType = .automobile
        
        let directions = MKDirections(request: request)
        directions.calculate { [weak self] (response, error) in
            guard let self = self else { return }
            guard let response = response else {
                print(error.debugDescription)
                return
            }
            self.route = response.routes.first
            
            self.mapView.addOverlay(self.route.polyline)
        }
    }
}

// MARK: UITextField Delegate Extension
extension HomeVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == destinationTextField {
            tableView.frame = CGRect(x: 20, y: view.frame.height, width: view.frame.width - 40, height: view.frame.height - 170)
            tableView.layer.cornerRadius = 5.0
            tableView.register(UITableViewCell.self, forCellReuseIdentifier: "locationCell")
            
            tableView.delegate = self
            tableView.dataSource = self
            
            tableView.tag = 18
            tableView.rowHeight = 60
            view.addSubview(tableView)
            animateTableView(shouldShow: true)
            
            UIView.animate(withDuration: 0.2) { [weak self] in
                guard let self = self else { return }
                self.destinationCircleView.backgroundColor = UIColor.red
                self.destinationCircleView.layer.borderColor = UIColor.init(displayP3Red: 199/255, green: 0/255, blue: 0/255, alpha: 1.0).cgColor
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == destinationTextField {
            performSearch()
            self.view.endEditing(true)
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == destinationTextField {
            if destinationTextField.text == "" {
                UIView.animate(withDuration: 0.2) { [weak self] in
                    guard let self = self else { return }
                    self.destinationCircleView.backgroundColor = UIColor.lightGray
                    self.destinationCircleView.layer.borderColor = UIColor.darkGray.cgColor
                }
            }
        }
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        centerMapOnUserLocation()
        matchingItems = []
        tableView.reloadData()
        
        return true
    }
    
    func animateTableView(shouldShow: Bool) {
        if shouldShow {
            UIView.animate(withDuration: 0.2) { [weak self] in
                guard let self = self else { return }
                self.tableView.frame = CGRect(x: 20, y: 170, width: self.view.frame.width - 40, height: self.view.frame.height - 170)
            }
        } else {
            UIView.animate(withDuration: 0.2, animations: {
                [weak self] in
                guard let self = self else { return }
                self.tableView.frame = CGRect(x: 20, y: self.view.frame.height, width: self.view.frame.width - 40, height: self.view.frame.height - 170)
            }) { [weak self] (completed) in
                guard let self = self else { return }
                if completed {
                    for subView in self.view.subviews {
                        if subView.tag == 18 {
                            subView.removeFromSuperview()
                        }
                    }
                }
            }
        }
    }
    
}

enum DistanceUnits {
    case km
    case meters
    case miles
}

// MARK: UITableView Delegate and DataSource Extension
extension HomeVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return matchingItems.count
    }
    
    func getDistanceBetweenTwoCoordinates(coordinate1: CLLocationCoordinate2D, coordinate2: CLLocationCoordinate2D, unit: DistanceUnits) -> Double {
        let currentLocation = CLLocation(latitude: coordinate1.latitude, longitude: coordinate1.longitude)
        let destnationLocation = CLLocation(latitude: coordinate2.latitude, longitude: coordinate2.longitude)
        
        switch unit{
        case .km:
            return destnationLocation.distance(from: currentLocation) / 1000.0
        case .meters:
            return destnationLocation.distance(from: currentLocation)
        case .miles:
            return destnationLocation.distance(from: currentLocation) / 1609.344
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "locationCell")
        let mapItem = matchingItems[indexPath.row]
        
        let currentLocationCoordinate = mapView.userLocation.coordinate
        let searchedLocationCoordinate = mapItem.placemark.coordinate
        
        let unit: DistanceUnits = .km
        
        let distane = getDistanceBetweenTwoCoordinates(coordinate1: currentLocationCoordinate, coordinate2: searchedLocationCoordinate, unit: unit)
        
        
        cell.textLabel?.text = "\(mapItem.name ?? "Unnamed Location") — \(String(format: "%.2f", distane)) \(unit) away"
        
        cell.detailTextLabel?.text = mapItem.placemark.title
        return cell
    }
    
    fileprivate func removePassengerAnnotations() {
        for annotation in mapView.annotations {
            if annotation.isKind(of: PassengerAnnotation.self) || annotation.isKind(of: DestinationAnnotation.self) {
                mapView.removeAnnotation(annotation)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let passengerCoordinate = manager?.location?.coordinate else {
            checkLocationAuthStatus()
            return
        }
        
        guard let uid = uid else { return }
        
        destinationTextField.text = tableView.cellForRow(at: indexPath)?.textLabel?.text
        
        let selectedMapItem = matchingItems[indexPath.row]
        removePassengerAnnotations()
        let passengerAnnotation = PassengerAnnotation(coordinate: passengerCoordinate, key: uid)
        let destinationAnnotation = DestinationAnnotation(coordinate: selectedMapItem.placemark.coordinate, key: uid)
        
        //        print(selectedMapItem.placemark.)
        
        DataService.instance.REF_USERS.child(uid).updateChildValues(["tripCoordinate" : [selectedMapItem.placemark.coordinate.latitude, selectedMapItem.placemark.coordinate.longitude]])
        
        self.mapView.addAnnotation(passengerAnnotation)
        self.mapView.addAnnotation(destinationAnnotation)
        self.removeExistingOverlays()
        searchMapKitForResultsWithPolyLine(for: selectedMapItem)
        
        // Hide tableView and keyboard
        animateTableView(shouldShow: false)
        self.view.endEditing(true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if destinationTextField.text == "" {
            animateTableView(shouldShow: false)
        }
    }
}
