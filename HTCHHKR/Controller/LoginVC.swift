//
//  LoginVC.swift
//  HTCHHKR
//
//  Created by Takasur Azeem on 20/10/2019.
//  Copyright © 2019 semangix. All rights reserved.
//

import UIKit
import Firebase

class LoginVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var emailField: RoundedCornerTextField!
    @IBOutlet weak var passwordField: RoundedCornerTextField!
    @IBOutlet weak var authButton: RoundedShadowButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        emailField.becomeFirstResponder()
        
        emailField.delegate = self
        passwordField.delegate = self
        
        view.bindToKeyboard()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleScreenTap(sender:)))
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func handleScreenTap(sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    @IBAction func dismisSelf(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet weak var authButtonTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var authButtonLeadingConstraint: NSLayoutConstraint!
    
    
    @IBAction func authButtonPressed(_ sender: Any) {
        if let email = emailField.text, !email.isEmpty, let password = passwordField.text, !password.isEmpty {
            authButton.startAnimate(spinnerType: .ballRotateChase, spinnercolor: .gray, complete: nil)
            self.view.endEditing(true)
            Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
                if error == nil, let user = Auth.auth().currentUser {
                    if self.segmentedControl.selectedSegmentIndex == 0 {
                        let userData = ["provider" : user.providerID] as [String : Any]
                        DataService.instance.createFirebaseDBUser(uid: user.uid, userData: userData, isDriver: false)
                    } else {
                        let userData = [
                            "provider" : user.providerID,
                            "userIsDriver" : true,
                            "isPickUpModeEnabled" : false,
                            "driverIsOnTrip" : false
                            ] as [String : Any]
                        DataService.instance.createFirebaseDBUser(uid: user.uid, userData: userData, isDriver: true)
                    }
                    print("Authenticated successfully with Firebase")
                    self.dismiss(animated: true, completion: nil)
                } else {
                    if let error = error, let errorCode = AuthErrorCode(rawValue: error._code) {
                        self.authButton.stopAnimate(complete: nil)
                        switch errorCode {
                        case .invalidEmail:
                            print("Invalid Email Address, please try again.")
                        case .wrongPassword:
                            print("Wrong password")
                        default:
                            print("\(error.localizedDescription)")
                        }
                        return
                    }
                    Auth.auth().createUser(withEmail: email, password: password) { (result, error) in
                        if let error = error, let errorCode = AuthErrorCode(rawValue: error._code) {
                            switch errorCode {
                            case .invalidEmail:
                                print("Invalid Email Address, please try again.")
                            case .accountExistsWithDifferentCredential:
                                print("You account already exists.")
                            case .emailAlreadyInUse:
                                print("Email already in use.")
                            case .weakPassword:
                                print("Weak password, choose a strong password")
                            default:
                                print("\(error.localizedDescription)")
                            }
                        } else {
                            if let user = Auth.auth().currentUser {
                                if self.segmentedControl.selectedSegmentIndex == 0 {
                                    let userData = [
                                        "provider":user.providerID
                                        ] as [String:Any]
                                    DataService.instance.createFirebaseDBUser(uid: user.uid, userData: userData, isDriver: false)
                                } else {
                                    let userData = [
                                        "provider" : user.providerID,
                                        "userIsDriver" : true,
                                        "isPickUpModeEnabled" : false,
                                        "driverIsOnTrip" : false
                                        ] as [String : Any]
                                    DataService.instance.createFirebaseDBUser(uid: user.uid, userData: userData, isDriver: true)
                                }
                            }
                            print("Successfully created a user with firebase")
                            self.authButton.stopAnimate(complete: nil)
                            self.dismiss(animated: true, completion: nil)
                        }
                    }
                }
            }
        }
    }
}
