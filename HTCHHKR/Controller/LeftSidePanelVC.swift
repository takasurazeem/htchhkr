//
//  LeftSidePanelVC.swift
//  HTCHHKR
//
//  Created by Takasur Azeem on 14/10/2019.
//  Copyright © 2019 semangix. All rights reserved.
//

import UIKit
import Firebase

class LeftSidePanelVC: UIViewController {
    
    let appDelegte = AppDelegate.getAppDelegate()
    
    @IBOutlet weak var loginSignUpButton: UIButton!
    @IBOutlet weak var userEmailLabel: UILabel!
    @IBOutlet weak var accountTypeLabel: UILabel!
    @IBOutlet weak var profileImage: RoundImageView!
    @IBOutlet weak var pickupModeEnabledLabel: UILabel!
    @IBOutlet weak var pickupModeToggle: UISwitch!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        pickupModeToggle.isOn = false
        pickupModeEnabledLabel.isHidden = true
        pickupModeToggle.isHidden = true
        
        observePassengersAndDrivers()
        
        if Auth.auth().currentUser == nil {
            userEmailLabel.text = ""
            accountTypeLabel.text = ""
            profileImage.isHidden = true
            
            loginSignUpButton.setTitle("Sign Up / Login", for: .normal)
        } else {
            userEmailLabel.text = Auth.auth().currentUser?.email
            accountTypeLabel.text = ""
            profileImage.isHidden = false
            
            loginSignUpButton.setTitle("Logout", for: .normal)
        }
    }
    
    func observePassengersAndDrivers() {

        DataService.instance.REF_USERS.observeSingleEvent(of: .value) { [weak self] snapshot in
            if let snapshot = snapshot.children.allObjects as? [DataSnapshot], let self = self {
                for snap in snapshot {
                    if snap.key == Auth.auth().currentUser?.uid {
                        self.accountTypeLabel.text = "PASSENGER"
                    }
                }
            }
        }
        
        DataService.instance.REF_DRIVERS.observeSingleEvent(of: .value) { [weak self] snapshot in
            if let snapshot = snapshot.children.allObjects as? [DataSnapshot], let self = self {
                for snap in snapshot {
                    if snap.key == Auth.auth().currentUser?.uid {
                        self.accountTypeLabel.text = "DRIVER"
                        self.pickupModeEnabledLabel.isHidden = false
                        self.pickupModeToggle.isHidden = false

                        if let switchStatus = snap.childSnapshot(forPath: "isPickupModeEnabled").value as? Bool {
                            self.pickupModeToggle.isOn = switchStatus
                        }
                    }
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func pickupModeWasToggled(_ sender: UISwitch) {
        if sender.isOn {
            pickupModeEnabledLabel.text = "PICKUP MODE ENABLED"
        } else {
            pickupModeEnabledLabel.text = "PICKUP MODE DISABLED"
        }
        appDelegte.menuContainerVC.toggleLeftPanel()
        guard let uid = Auth.auth().currentUser?.uid else { return }
        DataService.instance.REF_DRIVERS.child(uid).updateChildValues(["isPickupModeEnabled" : sender.isOn])
    }
    
    
    @IBAction func loginSignUpButtonWasPressed(_ sender: UIButton) {
        if Auth.auth().currentUser != nil {
            do {
                try Auth.auth().signOut()
                userEmailLabel.text = ""
                accountTypeLabel.text = ""
                profileImage.isHidden = true
                pickupModeEnabledLabel.isHidden = true
                pickupModeToggle.isHidden = true
                loginSignUpButton.setTitle("Sign Up / Login", for: .normal)
            } catch (let error) {
                print(error.localizedDescription)
            }
        } else {
            guard let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as? LoginVC else { return }
            self.present(loginVC, animated: true, completion: nil)
        }
    }
}
