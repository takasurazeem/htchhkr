//
//  CenterVCDelegate.swift
//  HTCHHKR
//
//  Created by Takasur Azeem on 16/10/2019.
//  Copyright © 2019 semangix. All rights reserved.
//

import Foundation
import UIKit

protocol CenterVCDelegate {
    func toggleLeftPanel()
    func addLeftPanelViewController()
    func animateLeftPanel(shouldExpand: Bool)
}
